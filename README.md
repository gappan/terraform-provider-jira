# Get started

#### Introduction 
This terraform provider plugin currently manages users/groups resources in JIRA. 

To build and run the plugin:

```bash
git clone git@gitlab.com:gappan/terraform-provider-jira.git
dep ensure
go build -o terraform-provider-jira
terraform init
terraform plan
```
