package main

import (
	"fmt"
	"net/http"

	jira "github.com/gappan/go-jira"
	"github.com/hashicorp/terraform/helper/schema"
)

func resourceUser() *schema.Resource {
	return &schema.Resource{
		Create: resourceUserCreate,
		Read:   resourceUserRead,
		Update: resourceUserUpdate,
		Delete: resourceUserDelete,
		Exists: resourceUserExists,
		Importer: &schema.ResourceImporter{
			State: resourceUserImport,
		},

		Schema: map[string]*schema.Schema{
			"display_name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"time_zone": &schema.Schema{
				Type:     schema.TypeString,
				Default:  "Europe/Berlin",
				Optional: true,
			},
			"active": &schema.Schema{
				Type:     schema.TypeBool,
				Default:  true,
				Optional: true,
			},
		},
	}
}

func resourceUserCreate(d *schema.ResourceData, m interface{}) error {
	firstName := d.Get("name").(string)
	displayName := d.Get("display_name").(string)
	email := d.Get("email").(string)
	timeZone := d.Get("time_zone").(string)
	active := d.Get("active").(bool)

	config := m.(*Config)
	u := jira.User{
		Name:         firstName,
		EmailAddress: email,
		DisplayName:  displayName,
		TimeZone:     timeZone,
		Active:       active,
	}

	user, response, err := config.jiraClient.User.Create(&u)
	if err != nil {
		return fmt.Errorf("Unable to create a user: %s (%s)", response.Status, err)
	}

	d.SetId(user.Key)
	return nil
}

func resourceUserRead(d *schema.ResourceData, m interface{}) error {
	name := d.Get("name").(string)

	config := m.(*Config)

	users, _, err := config.jiraClient.User.Find(name)
	if err != nil {
		return err
	}

	var user *jira.User
	for _, u := range users {
		if name == u.Name {
			user = &u
			break
		}
	}
	if user == nil {
		d.SetId("")
		return nil
	}

	d.Set("name", user.Name)
	d.Set("display_name", user.DisplayName)
	d.Set("email", user.EmailAddress)
	d.Set("time_zone", user.TimeZone)
	d.Set("active", user.Active)

	return nil
}

func resourceUserUpdate(d *schema.ResourceData, m interface{}) error {
	return fmt.Errorf("Not implemented")
}

func resourceUserDelete(d *schema.ResourceData, m interface{}) error {
	name := d.Get("name").(string)

	config := m.(*Config)

	response, err := config.jiraClient.User.Delete(name)
	if err != nil || response.StatusCode != http.StatusNoContent {
		return fmt.Errorf("Unable to delete a user: %s", response.Status)
	}

	return nil
}

func resourceUserExists(d *schema.ResourceData, m interface{}) (b bool, e error) {
	name := d.Get("name").(string)

	config := m.(*Config)

	if _, _, err := config.jiraClient.User.Get(name); err != nil {
		return false, err
	}
	return true, nil
}

func resourceUserImport(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	if err := resourceUserRead(d, meta); err != nil {
		return nil, err
	}
	return []*schema.ResourceData{d}, nil
}
