package main

import (
	"github.com/hashicorp/terraform/helper/schema"
	"github.com/pkg/errors"
)

// Provider returns a terraform.ResourceProvider
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"url": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("JIRA_URL", nil),
				Description: "Jira's base url",
			},
			"user": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("JIRA_USER", nil),
				Description: "Jira's user",
			},
			"password": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("JIRA_PASSWORD", nil),
				Description: "Jira's password",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"jira_user":  resourceUser(),
			"jira_group": resourceGroup(),
		},
		ConfigureFunc: providerConfigure,
	}
}

// providerConfigure configures the provider by creating and authenticating JIRA client
func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	url := d.Get("url").(string)
	user := d.Get("user").(string)
	password := d.Get("password").(string)

	var c Config
	if err := c.createAndAuthenticateClient(url, user, password); err != nil {
		return nil, errors.Wrap(err, "creating config failed")
	}
	return &c, nil
}
