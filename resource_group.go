package main

import (
	"fmt"

	jira "github.com/gappan/go-jira"
	"github.com/hashicorp/terraform/helper/schema"
)

func resourceGroup() *schema.Resource {
	return &schema.Resource{
		Create: resourceGroupCreate,
		Read:   resourceGroupRead,
		Update: resourceGroupUpdate,
		Delete: resourceGroupDelete,
		Exists: resourceGroupExists,
		Importer: &schema.ResourceImporter{
			State: resourceGroupImport,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"users": &schema.Schema{
				Type:     schema.TypeList,
				Elem:     &schema.Schema{Type: schema.TypeString},
				Optional: true,
			},
		},
	}
}

func resourceGroupCreate(d *schema.ResourceData, m interface{}) error {
	groupName := d.Get("name").(string)
	users := expandStringList(d.Get("users").([]interface{}))
	config := m.(*Config)
	g := jira.Group{
		Name: groupName,
	}

	group, response, err := config.jiraClient.Group.Create(&g)
	if err != nil {
		return fmt.Errorf("Unable to create a group: %s", response.Status)
	}
	// If users list is not nil add them to the group
	if len(users) > 0 {
		for _, u := range users {
			_, _, err := config.jiraClient.Group.Add(groupName, u)
			if err != nil {
				return fmt.Errorf("Issue adding %s to the group", u)
			}

		}
	}

	d.SetId(group.Name)
	return nil

}
func resourceGroupRead(d *schema.ResourceData, m interface{}) error {
	// name := d.Get("name").(string)

	// config := m.(*Config)

	// group, _, err := config.jiraClient.Group.Get(name)
	// if err != nil {
	// 	return err
	// }

	return nil
}

func resourceGroupUpdate(d *schema.ResourceData, m interface{}) error {
	return nil
}

func resourceGroupDelete(d *schema.ResourceData, m interface{}) error {
	name := d.Get("name").(string)

	config := m.(*Config)

	response, err := config.jiraClient.Group.Delete(name)
	if err != nil {
		return fmt.Errorf("Unable to delete a group: %s", response.Status)
	}

	return nil
}

func resourceGroupExists(d *schema.ResourceData, m interface{}) (b bool, e error) {
	config := m.(*Config)

	if _, _, err := config.jiraClient.Group.Get(d.Id()); err != nil {
		return false, err
	}
	return true, nil
}

func resourceGroupImport(d *schema.ResourceData, meta interface{}) ([]*schema.ResourceData, error) {
	if err := resourceGroupRead(d, meta); err != nil {
		return nil, err
	}
	return []*schema.ResourceData{d}, nil
}

func expandStringList(configured []interface{}) []string {
	vs := make([]string, 0, len(configured))
	for _, v := range configured {
		val, ok := v.(string)
		if ok && val != "" {
			vs = append(vs, val)
		}
	}
	return vs
}
