package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	jira "github.com/gappan/go-jira"
	"github.com/hashicorp/terraform/helper/schema"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestResourceUserCreate(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	data := map[string]interface{}{
		"name":         "john_doe",
		"display_name": "John Doe",
		"email":        "john_doe@example.com",
		"time_zone":    "Asia/Magadan",
		"active":       true,
	}

	resourceData := schema.TestResourceDataRaw(t, resourceUser().Schema, data)

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(r.Method, "POST")

		body, err := ioutil.ReadAll(r.Body)
		require.NoError(err)

		user := new(jira.User)
		err = json.Unmarshal(body, user)
		require.NoError(err)

		assert.Equal(user.Name, data["name"])
		assert.Equal(user.DisplayName, data["display_name"])
		assert.Equal(user.EmailAddress, data["email"])
		assert.Equal(user.TimeZone, data["time_zone"])
		assert.Equal(user.Active, data["active"])

		user.Key = "john_doe_key"

		respData, err := json.Marshal(user)
		require.NoError(err)

		fmt.Fprintln(w, string(respData))
	}))
	defer ts.Close()

	client, err := jira.NewClient(nil, ts.URL)
	require.NoError(err)
	config := &Config{jiraClient: client}

	err = resourceUserCreate(resourceData, config)
	assert.NoError(err)

	assert.Equal(resourceData.Id(), "john_doe_key")
}

func TestResourceUserRead(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	data := map[string]interface{}{
		"name":         "john_doe",
		"display_name": "John Doe",
		"email":        "john_doe@example.com",
		"time_zone":    "Asia/Magadan",
		"active":       true,
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(r.Method, "GET")

		query := r.URL.Query()
		assert.Equal(query.Get("username"), data["name"])

		user := jira.User{
			Name:         data["name"].(string),
			DisplayName:  data["display_name"].(string),
			EmailAddress: data["email"].(string),
			TimeZone:     data["time_zone"].(string),
			Active:       data["active"].(bool),
		}

		respData, err := json.Marshal([]jira.User{user})
		require.NoError(err)

		fmt.Fprintln(w, string(respData))
	}))
	defer ts.Close()

	resourceData := schema.TestResourceDataRaw(t, resourceUser().Schema, data)

	client, err := jira.NewClient(nil, ts.URL)
	require.NoError(err)
	config := &Config{jiraClient: client}

	err = resourceUserRead(resourceData, config)
	assert.NoError(err)

	assert.Equal(resourceData.Get("display_name"), data["display_name"])
	assert.Equal(resourceData.Get("name"), data["name"])
	assert.Equal(resourceData.Get("email"), data["email"])
	assert.Equal(resourceData.Get("time_zone"), data["time_zone"])
	assert.Equal(resourceData.Get("active"), data["active"])
}
