package main

import (
	"testing"

	"github.com/hashicorp/terraform/helper/resource"
)

func TestAccGroup_basic(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: `
					resource "jira_group" "group_from_acceptance_test" {
					  name         = "acc_test_group"
					}
				`,
			},
		},
	})
}
