package main

import (
	"log"

	jira "github.com/gappan/go-jira"
	"github.com/pkg/errors"
)

type Config struct {
	jiraClient *jira.Client
}

func (c *Config) createAndAuthenticateClient(url, user, password string) error {
	log.Printf("[INFO] creating jira client using environment variables")
	jiraClient, err := jira.NewClient(nil, url)
	if err != nil {
		return errors.Wrap(err, "creating jira client failed")
	}
	jiraClient.Authentication.SetBasicAuth(user, password)

	c.jiraClient = jiraClient
	return nil
}
