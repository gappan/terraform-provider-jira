package main

import (
	"testing"

	"github.com/hashicorp/terraform/helper/resource"
)

func TestAccUser_basic(t *testing.T) {
	resource.Test(t, resource.TestCase{
		PreCheck:  func() { testAccPreCheck(t) },
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: `
					resource "jira_user" "user_from_acceptance_test" {
					  display_name = "Acceptance Test"
					  name         = "acc_test"
					  email        = "acc_test@example.com"
					  time_zone    = "Europe/Berlin"
					  active       = "true"
					}
				`,
			},
		},
	})
}
